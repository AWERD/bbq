<?php

namespace api\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/api/web';
    public $css = [
        'css/header.min.css',
        'css/main.min.css',
        'css/swiper.min.css'
    ];
    public $js = [
        'js/js/swiper.min.js',
        'js/common.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyAfS_4_tPY7zFaLTVnHdY9QmyYGKU3TZxE&callback=initMap'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
