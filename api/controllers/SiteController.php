<?php

namespace api\controllers;

use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\web\Response;
use yii\rest\ActiveController;
use common\models\User;

class SiteController extends ActiveController
{
    public $modelClass = 'common\models\User';

    protected function verbs()
    {
        return [
            'test' => ['GET'],
        ];
    }
    public function behaviors() {
        $behaviors = parent::behaviors();
//        $behaviors['contentNegotiator']['formats']['text/html'] = Response::FORMAT_JSON;
//        $behaviors['authenticator'] = [
//            'class' => HttpBearerAuth::className()
//        ];
        return $behaviors;
    }

    public function actionTest()
    {
        return [
            'status' => 200,
            'data' => [
                'name' => 'Vasya',
                'age' => 23
            ]
        ];
    }
}