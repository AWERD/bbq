<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '/frontend/web';
    public $css = [
        'css/header.min.css',
        'css/main.min.css',
        'css/swiper.min.css',
        'css/fonts.min.css',
    ];
    public $js = [
        'js/swiper.min.js',
        'js/common.js',
        'js/jquery.fancybox.min.js',
        'js/jquery.sticky.js',
        'https://maps.googleapis.com/maps/api/js?key=AIzaSyAfS_4_tPY7zFaLTVnHdY9QmyYGKU3TZxE&callback=initMap'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
