<?php
namespace frontend\widgets\delivery;

use yii\base\Widget;

class DeliveryWidget extends Widget
{
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    return $this->render('delivery');
  }
}