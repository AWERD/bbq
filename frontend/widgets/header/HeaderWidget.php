<?php
namespace frontend\widgets\header;

use yii\base\Widget;

class HeaderWidget extends Widget
{
	public $headerClass;
  public $slogan;

  public function init()
  {
    parent::init();
    if ($this->headerClass === null) {
        $this->headerClass = 'default';
    }
  }

  public function run()
  {
    return $this->render('header', [
    	'class' => $this->headerClass,
      'slogan' => $this->slogan
    ]);
  }
}