<section class="bbq-foodmenu">
  <div class="container">
    <nav class="navbar">
     <div class="container-fluid">
       <div class="navbar-header">
         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
           <span class="icon-bar"></span>
         </button>
       </div>
       <div class="collapse navbar-collapse" id="myNavbar">
         <ul class="nav navbar-nav">
           <ul class="foodmenu">
             <?php if(isset($categories)): ?>
                 <?php foreach($categories as $key => $category): ?>
                     <li>
                      <a href="">
                        <img src="<?= $category->picture ?>" alt="<?= $category->picture_alt ?>">
                        <span><?= $category->name ?></span>
                      </a>
                     </li>
                 <?php endforeach; ?>
             <?php endif; ?>
          </ul>
         </ul>
       </div>
     </div>
   </nav>
  </div>
</section>