<?php
namespace frontend\widgets\category;

use yii\base\Widget;
use yii\caching\TagDependency;
use common\models\Category;

class CategoryWidget extends Widget
{
    public $model;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        return $this->render('category', [
            'categories' => $this->Query()
        ]);
    }

    protected function Query()
    {
        $cached_query = Category::getDb()->cache(function ($db) {
            return Category::find()
                ->where(['status' => 1])
                ->orderBy(['created_at' => SORT_DESC])
                ->all();
        }, 0, new TagDependency(['tags' => ['category', 'product']]));

        return $cached_query;
    }
}