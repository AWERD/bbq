<?php
namespace frontend\widgets\cart;

use yii\base\Widget;

class CartWidget extends Widget
{
  public function init()
  {
    parent::init();
  }

  public function run()
  {
    return $this->render('cart');
  }
}