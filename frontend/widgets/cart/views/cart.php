<div class="menu_icon ic-cart-custom">
    <i class="fa fa-bars" aria-hidden="true"></i>
</div>


<div class="menu-mobile--itself">

    <!--BUTTON-CLOSE -->
    <div class="close">
        <i class="fa fa-times-circle" ></i>
    </div>

    <div class="overflowtest">
    <h2 class="cart-header cart-static">Корзина</h2>
    <h2 class="cart-header cart-count">3 позиции</h2>
    <form action="" class="cart-items">

        <fieldset class="cart-singleitem">
            <img class="item-image" src="/img/cart-01.jpg" alt="cart-item-1">
            <p class="item-name">
                Сыр на мангале с лимонно-медовой заправкой
                <span>29 грн</span>
            </p>
            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>

            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" class="cartitem-close">
                    <path class="cartitem-closeico" fill="#C0C0C0" fill-rule="nonzero" d="M8.822 7.502l5.892-5.892A.936.936 0 1 0 13.39.286L7.498 6.178 1.606.286A.936.936 0 0 0 .282 1.61l5.892 5.892-5.892 5.892a.936.936 0 1 0 1.324 1.324l5.892-5.892 5.892 5.892a.934.934 0 0 0 1.324 0 .936.936 0 0 0 0-1.324L8.822 7.502z"/>
            </svg>
        </fieldset>

        <fieldset class="cart-singleitem">
            <img class="item-image" src="/img/cart-03.png" alt="cart-item-2">
            <p class="item-name">
                Большой денер
                <span>39 грн</span>
            </p>
            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>

            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" class="cartitem-close">
                    <path class="cartitem-closeico" fill="#C0C0C0" fill-rule="nonzero" d="M8.822 7.502l5.892-5.892A.936.936 0 1 0 13.39.286L7.498 6.178 1.606.286A.936.936 0 0 0 .282 1.61l5.892 5.892-5.892 5.892a.936.936 0 1 0 1.324 1.324l5.892-5.892 5.892 5.892a.934.934 0 0 0 1.324 0 .936.936 0 0 0 0-1.324L8.822 7.502z"/>
            </svg>
        </fieldset>


        <fieldset class="cart-singleitem">
            <img class="item-image" src="/img/cart-02.png" alt="cart-item-3">
            <p class="item-name">
                Куриный бургер-сет
                <span>60 грн</span>
            </p>
            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>

            <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" class="cartitem-close">
                    <path class="cartitem-closeico" fill="#C0C0C0" fill-rule="nonzero" d="M8.822 7.502l5.892-5.892A.936.936 0 1 0 13.39.286L7.498 6.178 1.606.286A.936.936 0 0 0 .282 1.61l5.892 5.892-5.892 5.892a.936.936 0 1 0 1.324 1.324l5.892-5.892 5.892 5.892a.934.934 0 0 0 1.324 0 .936.936 0 0 0 0-1.324L8.822 7.502z"/>
            </svg>
        </fieldset>

        <fieldset class="order-contact">
            <p><label for="order-name">Имя</label><input type="text" class="order-name" name="order-name" placeholder="Введите свое имя"></p>
            <p><label for="order-name">Телефон</label><input type="tel" class="order-phone" name="order-phone" placeholder="Введите свой телефон"></p>
            <p>
                <label>Способ оплаты</label>
                <select class="order-select">
                    <option>Наличными</option>
                    <option>Безналичный расчет</option>
                </select>
        </p>
        <p>
            <label>Способ доставки</label>
            <select class="order-select">
                <option>Самовывоз</option>
                <option>Доставка курьером</option>
            </select>
    </p>
        </fieldset>
        <fieldset class="deliverytime">
            time delivery block
        </fieldset>

        <p class="freedelivery-block">Закажи еще на 123 грн и получи бесплатную доставку!</p>
        <input type="submit" id="order-submit" value="Оформить заказ" class="cart-ordersubmit">
    </form>

    </div>
</div>