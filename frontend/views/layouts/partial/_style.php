<style>.unpadding {
        padding: 0 !important
    }

    a {
        outline: 0
    }

    .outlinenone {
        outline: 0
    }

    .outlinenone:active, .outlinenone:focus, .outlinenone:hover, .outlinenone:visited {
        outline: 0
    }

    .lastli-unmargin li:last-of-type {
        margin-right: 0
    }

    .btn-style {
        background-color: #10b4a4;
        -webkit-box-shadow: 0 3px 0 0 #0a6a60;
        box-shadow: 0 3px 0 0 #0a6a60;
        min-height: 48px;
        font: 600 20px/48px AvenirNext, sans-serif;
        text-align: center;
        color: #fff
    }

    .callback-form input[type=submit], .cart-ordersubmit {
        background-color: #10b4a4;
        -webkit-box-shadow: 0 3px 0 0 #0a6a60;
        box-shadow: 0 3px 0 0 #0a6a60;
        min-height: 48px;
        font: 600 20px/48px AvenirNext, sans-serif;
        text-align: center;
        color: #fff;
        border: none
    }

    @font-face {
        font-family: AvenirNext;
        src: url(../fonts/Avenir/AvenirNextCyr-Bold.eot);
        src: url(../fonts/Avenir/AvenirNextCyr-Bold.eot?#iefix) format("embedded-opentype"), url(../fonts/Avenir/AvenirNextCyr-Bold.woff) format("woff"), url(../fonts/Avenir/AvenirNextCyr-Bold.ttf) format("truetype"), url(../fonts/Avenir/AvenirNextCyr-Bold.svg#AvenirNext) format("svg");
        font-weight: 700;
        font-style: normal
    }

    @font-face {
        font-family: AvenirNext;
        src: url(../fonts/Avenir/AvenirNextCyrDemi.eot);
        src: url(../fonts/Avenir/AvenirNextCyrDemi.eot?#iefix) format("embedded-opentype"), url(../fonts/Avenir/AvenirNextCyrDemi.woff) format("woff"), url(../fonts/Avenir/AvenirNextCyrDemi.ttf) format("truetype"), url(../fonts/Avenir/AvenirNextCyrDemi.svg#AvenirNext) format("svg");
        font-weight: 600;
        font-style: normal
    }

    @font-face {
        font-family: AvenirNext;
        src: url(../fonts/Avenir/AvenirNextCyrMedium.eot);
        src: url(../fonts/Avenir/AvenirNextCyrMedium.eot?#iefix) format("embedded-opentype"), url(../fonts/Avenir/AvenirNextCyrMedium.woff) format("woff"), url(../fonts/Avenir/AvenirNextCyrMedium.ttf) format("truetype"), url(../fonts/Avenir/AvenirNextCyrMedium.svg#AvenirNext) format("svg");
        font-weight: 500;
        font-style: normal
    }

    @font-face {
        font-family: AvenirNext;
        src: url(../fonts/Avenir/AvenirNextCyr-Regular.eot);
        src: url(../fonts/Avenir/AvenirNextCyr-Regular.eot?#iefix) format("embedded-opentype"), url(../fonts/Avenir/AvenirNextCyr-Regular.woff) format("woff"), url(../fonts/Avenir/AvenirNextCyr-Regular.ttf) format("truetype"), url(../fonts/Avenir/AvenirNextCyr-Regular.svg#AvenirNext) format("svg");
        font-weight: 400;
        font-style: normal
    }

    .custom-topnav {
        -webkit-box-sizing: border-box;
        box-sizing: border-box
    }

    .custom-topnav li {
        font: 500 15px/20px AvenirNext, sans-serif;
        color: #fff
    }

    .logo-center {
        margin: 0 140px
    }

    .header-phone {
        margin-right: 94px;
        background: url(../img/incoming-call.svg) no-repeat scroll left center;
        padding-left: 30px
    }

    .work-time {
        margin-right: 45px;
        background: url(../img/clock.svg) no-repeat scroll left center;
        padding-left: 30px
    }

    header #navbar-primary .navbar-nav > li > a {
        padding-left: 0;
        padding-right: 0;
        font: 500 15px/20px AvenirNext, sans-serif;
        color: #fff
    }

    #menu {
        margin-top: 26px
    }

    #callback {
        text-decoration: none;
        border-bottom: 1px dashed #fff;
        padding: 0
    }

    #callback:hover {
        background: 0 0
    }

    .slogan-top {
        font: 500 36px/39px AvenirNext, sans-serif;
        text-align: center;
        color: #fff;
        margin-top: 14%
    }

    .top-buttons-wrap {
        display: table;
        margin: 40px auto 0
    }

    .top-buttons-wrap a {
        min-width: 173px;
        padding: 0 41px;
        display: table;
        float: left;
        -webkit-box-sizing: border-box;
        box-sizing: border-box
    }

    .top-buttons-wrap a:first-of-type {
        margin-right: 24px
    }

    .top-buttons-wrap a:hover {
        color: #fff;
        text-decoration: none
    }

    .cart-header {
        font: 600 24px/26px AvenirNext, sans-serif;
        margin-bottom: 30px
    }

    .cart-static {
        display: table;
        float: left;
        color: #474747
    }

    .cart-count {
        display: table;
        float: right;
        color: #d8d8d8;
        position: relative
    }

    .cart-items {
        width: 100%;
        display: table
    }

    .cart-singleitem {
        border-bottom: dashed 1px #d8d8d8;
        padding-bottom: 11px;
        margin-top: 11px;
        position: relative
    }

    .cart-singleitem .item-image {
        float: left;
        display: block;
        width: 65px;
        height: 65px;
        object-fit: cover;
        -webkit-border-radius: 5px;
        border-radius: 5px
    }

    .cart-singleitem .item-name {
        width: 100%;
        max-width: 230px;
        float: left;
        margin-left: 25px
    }

    .cart-singleitem .item-name span {
        width: 100%;
        font: 600 16px/16px AvenirNext, sans-serif;
        text-align: left;
        color: #474747;
        display: block;
        margin-top: 9px
    }

    .cart-singleitem .cartitem-close {
        width: 15px;
        height: 15px;
        object-fit: contain;
        display: table;
        cursor: pointer;
        margin: auto;
        position: absolute;
        right: 0;
        top: 10px
    }

    .cart-singleitem .cartitem-close .cartitem-closeico {
        fill: silver
    }

    .order-contact {
        display: table;
        width: 100%;
        margin-top: 20px
    }

    .order-contact p {
        width: 50%;
        float: left;
        display: table
    }

    .order-contact p label {
        display: block
    }

    .order-contact p input {
        max-width: 215px;
        width: 100%;
        height: 35px;
        padding: 0 10px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        border: solid 1px #d8d8d8;
        font: 400 14px/35px AvenirNext, sans-serif;
        text-align: left;
        color: #474747
    }

    .order-contact p input::-moz-placeholder {
        color: #d8d8d8
    }

    .order-contact p input::-webkit-input-placeholder {
        color: #d8d8d8
    }

    .order-contact p input:-ms-input-placeholder {
        color: #d8d8d8
    }

    .order-contact p input::placeholder {
        color: #d8d8d8
    }

    .order-contact p label {
        font: 500 14px/15px AvenirNext, sans-serif;
        color: #474747
    }

    .freedelivery-block {
        width: 100%;
        min-height: 54px;
        background: rgba(16, 180, 164, .1) url(../img/delivery-truck.svg) no-repeat scroll 15px center;
        padding-left: 60px;
        border-top: dashed 1px #d8d8d8;
        border-bottom: dashed 1px #d8d8d8;
        font: 600 14px/54px AvenirNext, sans-serif;
        text-align: left;
        color: #0a6a60;
        margin-top: 21px
    }

    .counter {
        display: table
    }

    .counter input {
        width: 35px;
        height: 35px;
        border: solid 1px #d8d8d8;
        font: 500 16px/35px AvenirNext, sans-serif;
        text-align: center;
        color: #474747;
        -webkit-appearance: none;
        background: 0 0
    }

    .counter button {
        width: 25px;
        height: 35px;
        border: none;
        background: 0 0;
        font: 500 16px/35px AvenirNext, sans-serif;
        text-align: center;
        color: #10b4a4
    }

    .callback-form input[type=tel], .callback-form input[type=text] {
        height: 48px;
        -webkit-border-radius: 5px;
        border-radius: 5px;
        background-color: #fff;
        border: solid 1px #d8d8d8;
        margin-right: 25px;
        -webkit-box-sizing: border-box;
        box-sizing: border-box;
        padding: 0 21px;
        font: 400 15px/48px AvenirNext, sans-serif;
        color: #474747;
        width: 30%
    }

    .callback-form input[type=submit] {
        width: 100%;
        max-width: 275px
    }

    .callback-form #callback-close {
        display: inline-block;
        width: 20px;
        height: 20px;
        margin-top: 30px;
        margin-left: 30px;
        cursor: pointer
    }

    .cart-ordersubmit {
        width: 100%;
        position: relative;
        margin-top: 20px
    }

    #navbar-primary .navbar-nav {
        width: 100%;
        text-align: center
    }

    #navbar-primary .navbar-nav > li {
        display: inline-block;
        float: none
    }

    #navbar-primary .navbar-nav > li > a {
        padding-left: 30px;
        padding-right: 30px
    }</style>