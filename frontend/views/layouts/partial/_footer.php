<?php
use yii\helpers\Html;
?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-6">
                <ul>
                    <li><?= Html::a('Меню', ['']) ?></li>
                    <li><?= Html::a('Ресторан', ['']) ?></li>
                    <li><?= Html::a('Бизнес меню', ['']) ?></li>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-6">
                <ul>
                    <?php if(!empty($phones = Yii::$app->siteSettings->phones())): ?>
                        <?php foreach($phones as $phone): ?>
                            <li><?= Html::a(substr($phone->phone, 3), ['#']) ?></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-6">
                <ul class="footer-social">
                    <?= $settings->instagram_status == 1 ?
                        '<li><a href="'.$settings->instagram_url.'" target="_blank"><img src="/img/instagram.svg" alt="footer-instagram" title="instagram"></a></li>' : '';
                    ?>
                    <?= $settings->facebook_status == 1 ?
                        '<li><a href="'.$settings->facebook_url.'" target="_blank"><img src="/img/facebook.svg" alt="footer-facebook" title="facebook"></a></li>' : '';
                    ?>
                </ul>
            </div>
            <div class="col-sm-6 col-lg-3 col-md-3 col-xs-6">
                <a href="#" target="_blank"><img src="/img/logo-footer.svg" alt="urich-logo" class="urich-logo" title="urich-logo"></a>
            </div>
        </div>
    </div>
</footer>