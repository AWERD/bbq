<?php

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use frontend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
$settings = Yii::$app->siteSettings->data();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>

    <!-- Load CSS, CSS Localstorage & WebFonts Main Function -->
    <script>!function(e){"use strict";function t(t,n){return e.localStorage&&localStorage[t+"_content"]&&localStorage[t+"_file"]===n}function n(n,o){if(e.localStorage&&e.XMLHttpRequest)t(n,o)?a(localStorage[n+"_content"]):function(e,t){var n=new XMLHttpRequest;n.open("GET",t,!0),n.onreadystatechange=function(){4===n.readyState&&200===n.status&&(a(n.responseText),localStorage[e+"_content"]=n.responseText,localStorage[e+"_file"]=t)},n.send()}(n,o);else{var r=l.createElement("link");r.href=o,r.id=n,r.rel="stylesheet",r.type="text/css",l.getElementsByTagName("head")[0].appendChild(r),l.cookie=n}}function a(e){var t=l.createElement("style");t.setAttribute("type","text/css"),l.getElementsByTagName("head")[0].appendChild(t),t.styleSheet?t.styleSheet.cssText=e:t.innerHTML=e}var l=e.document;e.loadCSS=function(e,t,n){var a,o=l.createElement("link");if(t)a=t;else{var r;a=(r=l.querySelectorAll?l.querySelectorAll("style,link[rel=stylesheet],script"):(l.body||l.getElementsByTagName("head")[0]).childNodes)[r.length-1]}var s=l.styleSheets;o.rel="stylesheet",o.href=e,o.media="only x",a.parentNode.insertBefore(o,t?a:a.nextSibling);var c=function(e){for(var t=o.href,n=s.length;n--;)if(s[n].href===t)return e();setTimeout(function(){c(e)})};return o.onloadcssdefined=c,c(function(){o.media=n||"all"}),o},e.loadLocalStorageCSS=function(a,o){t(a,o)||l.cookie.indexOf(a)>-1?n(a,o):("load",s=function(){n(a,o)},(r=e).addEventListener?r.addEventListener("load",s,!1):r.attachEvent&&r.attachEvent("onload",s));var r,s}}(this)</script>

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

    <?= $content ?>



    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <ul class="footer-list">
                        <li>
                            <a href="menu.html">Меню</a>
                        </li>
                        <li>
                            <a href="restaurant.html">Ресторан</a>
                        </li>
                        <li>
                            <a href="business-menu.html">Бизнес меню</a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <ul class="footer-list">
                        <li><a href="#">(066) 451-36-74</a></li>
                        <li><a href="#">(066) 451-36-74</a></li>
                        <li><a href="#">(066) 451-36-74</a></li>
                    </ul>


                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <ul class="footer-social">
                        <li><a href="#"><img src="/img/instagram.svg" alt="footer-instagram" title="instagram"></a></li>
                        <li><a href="#"><img src="/img/facebook.svg" alt="footer-facebook" title="facebook"></a></li>
                    </ul>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                    <a href="#" class="urich-logo">
                        <img src="/img/logo-footer.svg" alt="urich-logo" title="urich-logo">
                        <span>Designed by URich</span>
                    </a>
                </div>
            </div>
        </div>
    </footer>
    <div class="main-overlay"></div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
