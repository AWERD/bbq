<?php

use frontend\widgets\category\CategoryWidget;
use frontend\widgets\cart\CartWidget;
use frontend\widgets\delivery\DeliveryWidget;

$this->title = 'BBQ Запорожье';
?>

<div id="trailer" class="is_overlay">
    <video id="video" width="100%" height="auto" autoplay="autoplay" loop="loop" preload="auto">
        <source src="video/Bbq-medium.mp4"></source>
        <source src="video/plane.webm" type="video/webm"></source>
    </video>
    <div class="section-overlay"></div>
    <header role="banner">
        <nav id="navbar-primary" class="navbar" role="navigation">
            <div class="container-fluid nopadding">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="searchblock">
                    <form action="" class="search-form">
                        <fieldset>
                            <input type="text" placeholder="Введите название блюда или ингридиента, например Четыре сыра" name="search-field" class="search-field">
                            <input type="submit" id="search-submit" value="">
                        </fieldset>
                    </form>
                </div>
                <div class="callback-block">
                    <div class="container">
                        <form action="" class="callback-form">
                                <input type="text" placeholder="Ваше имя" name="callback-name" class="callback-name">
                                <input type="tel" placeholder="Ваш телефон" name="callback-phone" class="callback-phone" pattern="2[0-9]{3}-[0-9]{3}">
                                <input type="submit" id="callback-submit" value="Заказать обратный звонок">
                                <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" id="callback-close">
                                        <path class="cartitem-closeico" fill="#C0C0C0" fill-rule="nonzero" d="M8.822 7.502l5.892-5.892A.936.936 0 1 0 13.39.286L7.498 6.178 1.606.286A.936.936 0 0 0 .282 1.61l5.892 5.892-5.892 5.892a.936.936 0 1 0 1.324 1.324l5.892-5.892 5.892 5.892a.934.934 0 0 0 1.324 0 .936.936 0 0 0 0-1.324L8.822 7.502z"/>
                                </svg>
                        </form>

                    </div>
                </div>
                <div class="uncollapse" id="navbar-primary-collapse">
                    <ul class="nav navbar-nav custom-topnav" id="menu">
                        <li class="header-phone">(066) 451-36-74</li>
                        <li><a id="callback" href="#services-section-wrap">Заказать обратный звонок</a></li>
                        <li class="logo-center"><a href="index.html"><img src="img/bbq-logo.png" class="top-logo"></a></li>
                        <li class="work-time"><span>Работаем с 10:00 до 20:00</span></li>
                        <li class="header-social">
                            <ul class="additionalmenu-top">
                                <li><a class="insta-top" href="#"></a></li>
                                <li><a class="facebook-top" href="#"></a></li>
                                <li id="search">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                        <path class="search-ico" fill-rule="nonzero" d="M19.734 18.448l-5.16-5.16a8.182 8.182 0 1 0-1.286 1.285l5.16 5.16a.91.91 0 0 0 1.286-1.285zM8.182 14.545a6.364 6.364 0 1 1 0-12.727 6.364 6.364 0 0 1 0 12.727z"/>
                                    </svg>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
            <h1 class="slogan-top">Лучший ресторан Запорожья <br>с доставкой в любой район города</h1>
            <div class="top-buttons-wrap">
                <a class="btn-style" href="menu.html">Доставка</a>
                <a class="btn-style" href="restaurant.html">Ресторан</a>
            </div>
    </header>

</div>

 <?= CategoryWidget::widget() ?>

<section class="bannersection">

    <div class="container arrwrap">
        <div class="row">
        <a href="#" class="swiper-button-prev"></a>
        <a href="#" class="swiper-button-next"></a>
                <div class="swiper-container top_slider">
                    <div class="swiper-wrapper">
                        <div class="swiper-slide top_greenslide">
                                <a href="#"><img src="img/slider-pic.jpg" alt="slider-img slider-singleimg" title="розовый динозаврик" class="img-responsive"></a>
                        </div>

                        <div class="swiper-slide top_greenslide">
                                <a href="#"><img src="img/slider-pic.jpg" alt="slider-img slider-singleimg" title="розовый динозаврик" class="img-responsive"></a>
                        </div>

                        <div class="swiper-slide top_greenslide">
                                <a href="#"><img src="img/slider-pic.jpg" alt="slider-img slider-singleimg" title="розовый динозаврик" class="img-responsive"></a>
                        </div>
                    </div>
                </div>
        </div>
    </div>

</section>


<section class="popular-bbq">
    <div class="container nopadding">
        <h2 class="bbq-header recommended-bbq">Рекомендуемые блюда</h2>
    <div class="grid-wrap cols-three">
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="img/cart-01.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Сыр на мангале с лимонно-медовой заправкой</h3>
            <span class="recommended-price">29 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="img/cart-02.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Бургеры</span>
            <h3>Куриный бургер-сет</h3>
            <span class="recommended-price">60 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
    </div>
    </div>
</section>

<!-- GMAP KEY   AIzaSyAfS_4_tPY7zFaLTVnHdY9QmyYGKU3TZxE  -->

<?= DeliveryWidget::widget() ?>

<section class="mapsection">
    <div id="map"></div>
</section>


<?= CartWidget::widget() ?>





