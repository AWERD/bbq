<?php

use frontend\widgets\category\CategoryWidget;
use frontend\widgets\header\HeaderWidget;
use frontend\widgets\cart\CartWidget;
use frontend\widgets\delivery\DeliveryWidget;

$this->title = 'BBQ-Подписка на бизнес меню';
?>



<?= HeaderWidget::widget(['headerClass' => 'menupage-oneitem']) ?> 

<?= CategoryWidget::widget() ?>

<section class="bbq-breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs-top">
            <li><a href="index.html">Главная</a></li>
            <li><a href="menu.html">Меню</a></li>
            <li>Бургеры</li>
        </ul>
    </div>
</section>

<section class="bbq-singleitem">
    <div class="container clr-white">
        <div class="row">
        <form action="" class="form-corporatedinner">
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <fieldset class="name">
                    <label for="">Имя</label>
                    <input type="text" class="order-name" name="dinnerorder-name" placeholder="Введите свое имя">
                </fieldset>

                <fieldset class="phone">
                    <label for="">Телефон</label>
                    <input type="text" class="dinnerorder-phone" name="order-name" placeholder="Введите свой телефон">
                </fieldset>

                <fieldset class="email">
                    <label for="">E-mail</label>
                    <input type="email" class="dinnerorder-email" name="order-name" placeholder="example@mail.com">
                </fieldset>
                <fieldset class="corporatedinner-select payment-method">
                    <label for="">Способ оплаты</label>
                    <select class="order-select">
                        <option>Наличными</option>
                        <option>Безналичный расчет</option>
                    </select>
                </fieldset>
                <fieldset class="corporatedinner-select">
                    <label for="">Время доставки</label>
                    <select class="order-select">
                        <option>13:00</option>
                        <option>14:00</option>
                        <option>15:00</option>
                    </select>
                </fieldset>
                <fieldset class="dinner-textarea">
                        <label for="">Адрес доставки</label>
                        <textarea name="name" placeholder="Введите ваш адрес доставки"></textarea>
                </fieldset>

            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                <fieldset class="customradio">
                    <label for="">Вид подписки</label>
                    <span>
                        <input type="radio" id="dinner-oneweek" name="dinner-period">
                        <label for="dinner-oneweek">одна неделя</label>
                    </span>
                    <span>
                        <input type="radio" id="dinner-twoweek" name="dinner-period">
                        <label for="dinner-twoweek">две недели</label>
                    </span>
                    <span>
                        <input type="radio" id="dinner-month" name="dinner-period">
                        <label for="dinner-month">месяц</label>
                    </span>
                </fieldset>

                <fieldset class="dinner-itemcount">
                    <label for="">Количество комплектов</label>
                    <select class="order-select">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                        <option>6</option>
                        <option>7</option>
                        <option>8</option>
                        <option>9</option>
                        <option>10</option>
                    </select>
                </fieldset>

                <fieldset class="dinner-textarea">
                        <label for="">Комментарии</label>
                        <textarea name="comment" placeholder="Комментарии к заказу"></textarea>
                </fieldset>
                <fieldset class="complects-count complects-count-typo">
                    <span class="complects-items">5 комплектов</span>
                    <span class="complects-price">750 грн</span>
                </fieldset>
                <fieldset class="complects-count complects-count-payment">
                    <span class="complects-items">Итого к оплате</span>
                    <span class="complects-price">3750 грн</span>
                </fieldset>
                <input id="form-corporatedinner" value="Оформить заказ" class="corporatedinner-ordersubmit" type="submit">
            </div>
        </form>
        </div>
    </div>
</section>



<section class="popular-bbq">
    <div class="container nopadding">
        <h2 class="bbq-header recommended-bbq">Рекомендуемые блюда</h2>
    <div class="grid-wrap cols-three">
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-01.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Сыр на мангале с лимонно-медовой заправкой</h3>
            <span class="recommended-price">29 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-02.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Бургеры</span>
            <h3>Куриный бургер-сет</h3>
            <span class="recommended-price">60 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
    </div>
    </div>
</section>


<?= CartWidget::widget() ?>

<?= DeliveryWidget::widget() ?>




