<?php

use frontend\widgets\category\CategoryWidget;
use frontend\widgets\header\HeaderWidget;
use frontend\widgets\cart\CartWidget;
use frontend\widgets\delivery\DeliveryWidget;

$this->title = 'BBQ-Бизнес-меню';
?>



<?= HeaderWidget::widget(['headerClass' => 'business-menu', 'slogan' => 'Бизнес меню']) ?> 

<?= CategoryWidget::widget() ?>

<section class="bbq-breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs-top">
            <li><a href="index.html">Главная</a></li>
            <li><a href="menu.html">Меню</a></li>
            <li>Бургеры</li>
        </ul>
    </div>
</section>

<section class="bbq-singleitem">
    <div class="container clr-white">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <img src="/img/business-menu-pic.jpg" alt="bbq-oneitem" class="img-responsive singlebbq">
            </div>
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                <h2 class="bbqsingle-itemhead">Бизнес меню</h2>
                <p class="bbqsingle-item-text">У нас можно заказать бизнес-ланч с доставкой в ​​офис.
                    Для этого заранее свяжитесь с нами, чтобы мы успели позаботиться о вашем обеде.</p>
                <p class="bbqsingle-item-text">Согласитесь, ведь стоит лишь человеку хорошо поесть в обеденное время – как он сразу же становится добрее и
                    даже дела начинают идти в гору! Поэтому можно с уверенностью сказать, что бизнес-ланч – это еще и инвестиция в производительность.</p>
                <p class="bbqsingle-item-text">Меню бизнес-ланчей меняется так, чтобы не надоедать нашим гостям и соответствовать сезону.</p>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="bbq-period">
                    <div class="week-item">
                        <p>
                            Одна неделя
                            <span>750 грн</span>
                        </p>
                        <a href="business-menu-subscribe.html">Заказать</a>
                    </div>
                    <div class="week-item">
                        <p>
                            Две недели
                            <span>1250 грн</span>
                        </p>
                        <a href="business-menu-subscribe.html">Заказать</a>
                    </div>
                    <div class="week-item">
                        <p>
                            Месяц
                            <span>2000 грн</span>
                        </p>
                        <a href="business-menu-subscribe.html">Заказать</a>
                    </div>



                </div>
            </div>
        </div>
    </div>
</section>


<section class="popular-bbq">
    <div class="container nopadding">
        <h2 class="bbq-header recommended-bbq">Рекомендуемые блюда</h2>
    <div class="grid-wrap cols-three">
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-01.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive animfind"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Сыр на мангале с лимонно-медовой заправкой</h3>
            <span class="recommended-price">29 грн</span>
            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-02.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Бургеры</span>
            <h3>Куриный бургер-сет</h3>
            <span class="recommended-price">60 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
    </div>
    </div>
</section>


<?= CartWidget::widget() ?>

<?= DeliveryWidget::widget() ?>