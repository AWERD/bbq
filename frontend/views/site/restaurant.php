<?php

use frontend\widgets\category\CategoryWidget;
use frontend\widgets\header\HeaderWidget;
use frontend\widgets\cart\CartWidget;
use frontend\widgets\delivery\DeliveryWidget;

$this->title = 'BBQ-Ресторан';
?>



<?= HeaderWidget::widget(['headerClass' => 'menupage', 'slogan' => 'Бургеры']) ?> 

<?= CategoryWidget::widget() ?>

<section class="bbq-breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs-top">
            <li><a href="index.html">Главная</a></li>
            <li><a href="menu.html">Меню</a></li>
            <li>Бургеры</li>
        </ul>
    </div>
</section>

<section class="textsection">

    <div class="container textcont mainsection-text">

        <div class="swiper">
            <div class="swiper-container">

                    <a href="#" class="swiper-button-prev"></a>
                    <a href="#" class="swiper-button-next"></a>

                <div class="swiper-wrapper">
                    <div class="swiper-slide img-responsive"><img src="/img/bbq-slider1.jpg" alt="" title="d"></div>
                    <div class="swiper-slide img-responsive"><img src="/img/bbq-slider2.jpg" alt="" title="d"></div>
                    <div class="swiper-slide img-responsive"><img src="/img/bbq-slider3.jpg" alt="" title="d"></div>
                    <div class="swiper-slide img-responsive"><img src="/img/bbq-slider4.jpg" alt="" title="d"></div>
                </div>
            </div>

            <!-- Add Pagination -->
            <div class="swiper-thumbnails">
                        <span><img src="/img/bbq-slider1.jpg" alt="" title="d" class="img-responsive"></span>
                        <span><img src="/img/bbq-slider2.jpg" alt="" title="d" class="img-responsive"></span>
                        <span><img src="/img/bbq-slider3.jpg" alt="" title="d" class="img-responsive"></span>
                        <span><img src="/img/bbq-slider4.jpg" alt="" title="d" class="img-responsive"></span>

            </div>
        </div>




        Мы гарантируем, что все блюда, заказанные Вами, готовятся непосредственно перед отправкой. А чтобы вкус и аромат блюд сохранился с доставкой, мы используем специальную пищевую упаковку.
        <p>Доставка еды в Запорожье «Rock n Roll» это: вкусно, быстро и недорого. Именно на этих трех принципах работает Доставка еды на дом «Rock n Roll».</p>
        <p>1. Вкусно потому что: все наши суши и пицца готовятся только перед самой доставкой к вашему столу. Готовят их профессиональные повара из свежих и качественных ингредиентов, строго следуя уникальным рецептам «Rock n Roll»</p>
        <p>2. Мы доставляем суши и пиццу — максимально быстро. А все благодаря команде профессионалов : операторов, поваров, курьеров, логистов, упаковщиков. У которых одна общая цель — оперативная доставка вкусной еды от «Rock n Roll» прямо к Вашему столу.</p>
        <p>3. Мы делаем вкусные суши и пиццу с доставкой доступнее, каждый день, выбирая только лучшие продукты у самых надежных и крупных поставщиков с эксклюзивными ценами по всей Украине.</p>
        <p>Мы гарантируем, что все блюда, заказанные Вами, готовятся непосредственно перед отправкой. А чтобы вкус и аромат блюд сохранился с доставкой, мы используем специальную пищевую упаковку.</p>
    </div>
</section>

<section class="resaurant-order">

    <div class="container">
        <form action="" class="orderform center-block">
            <h2>Заказать столик</h2>
            <fieldset class="date">
                <label for="">Дата</label>
                <select class="order-select">
                    <option>29 сентября</option>
                    <option>9 сентября</option>
                    <option>14 марта</option>
                    <option>30 марта</option>
                </select>
            </fieldset>

            <fieldset class="time">
                <label for="">Время</label>
                <select class="order-select">
                    <option>14:30</option>
                    <option>14:30</option>
                    <option>14:30</option>
                    <option>14:30</option>
                </select>
            </fieldset>

            <fieldset class="place-quantity">
                <label for="">Количество мест</label>
                <select class="order-select">
                    <option>1</option>
                    <option>2</option>
                    <option>3</option>
                    <option>4</option>
                    <option>5</option>
                    <option>6</option>
                </select>
            </fieldset>

            <fieldset class="name">
                <label for="">Имя</label>
                <input type="text" class="order-name" name="order-name" placeholder="Введите свое имя">
            </fieldset>

            <fieldset class="phone">
                <label for="">Телефон</label>
                <input type="text" class="order-name" name="order-name" placeholder="Введите свой телефон">
            </fieldset>
            <fieldset class="submit">
                <input type="submit" value="Оставить заявку">
            </fieldset>
        </form>
    </div>
</section>


<section class="mapsection">
    <div id="map"></div>
</section>





<div class="menu-mobile--itself">

    <!--BUTTON-CLOSE -->
    <div class="close">
        <i class="fa fa-times-circle" ></i>
    </div>

<div class="overflowtest">
<h2 class="cart-header cart-static">Корзина</h2>
<h2 class="cart-header cart-count">3 позиции</h2>
<form action="" class="cart-items">

    <fieldset class="cart-singleitem">
        <img class="item-image" src="/img/cart-01.jpg" alt="cart-item-1">
        <p class="item-name">
            Сыр на мангале с лимонно-медовой заправкой
            <span>29 грн</span>
        </p>
        <div class="counter">
            <button type="button" class="but counterBut dec">-</button>
            <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
            <button type="button" class="but counterBut inc">+</button>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" class="cartitem-close">
                <path class="cartitem-closeico" fill="#C0C0C0" fill-rule="nonzero" d="M8.822 7.502l5.892-5.892A.936.936 0 1 0 13.39.286L7.498 6.178 1.606.286A.936.936 0 0 0 .282 1.61l5.892 5.892-5.892 5.892a.936.936 0 1 0 1.324 1.324l5.892-5.892 5.892 5.892a.934.934 0 0 0 1.324 0 .936.936 0 0 0 0-1.324L8.822 7.502z"/>
        </svg>
    </fieldset>

    <fieldset class="cart-singleitem">
        <img class="item-image" src="/img/cart-03.png" alt="cart-item-2">
        <p class="item-name">
            Большой денер
            <span>39 грн</span>
        </p>
        <div class="counter">
            <button type="button" class="but counterBut dec">-</button>
            <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
            <button type="button" class="but counterBut inc">+</button>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" class="cartitem-close">
                <path class="cartitem-closeico" fill="#C0C0C0" fill-rule="nonzero" d="M8.822 7.502l5.892-5.892A.936.936 0 1 0 13.39.286L7.498 6.178 1.606.286A.936.936 0 0 0 .282 1.61l5.892 5.892-5.892 5.892a.936.936 0 1 0 1.324 1.324l5.892-5.892 5.892 5.892a.934.934 0 0 0 1.324 0 .936.936 0 0 0 0-1.324L8.822 7.502z"/>
        </svg>
    </fieldset>


    <fieldset class="cart-singleitem">
        <img class="item-image" src="/img/cart-02.png" alt="cart-item-3">
        <p class="item-name">
            Куриный бургер-сет
            <span>60 грн</span>
        </p>
        <div class="counter">
            <button type="button" class="but counterBut dec">-</button>
            <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
            <button type="button" class="but counterBut inc">+</button>
        </div>

        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" class="cartitem-close">
                <path class="cartitem-closeico" fill="#C0C0C0" fill-rule="nonzero" d="M8.822 7.502l5.892-5.892A.936.936 0 1 0 13.39.286L7.498 6.178 1.606.286A.936.936 0 0 0 .282 1.61l5.892 5.892-5.892 5.892a.936.936 0 1 0 1.324 1.324l5.892-5.892 5.892 5.892a.934.934 0 0 0 1.324 0 .936.936 0 0 0 0-1.324L8.822 7.502z"/>
        </svg>
    </fieldset>

    <fieldset class="order-contact">
        <p><label for="order-name">Имя</label><input type="text" class="order-name" name="order-name" placeholder="Введите свое имя"></p>
        <p><label for="order-name">Телефон</label><input type="tel" class="order-phone" name="order-phone" placeholder="Введите свой телефон"></p>
        <p>
            <label>Способ оплаты</label>
            <select class="order-select">
                <option>Наличными</option>
                <option>Безналичный расчет</option>
            </select>
    </p>
    <p>
        <label>Способ доставки</label>
        <select class="order-select">
            <option>Самовывоз</option>
            <option>Доставка курьером</option>
        </select>
</p>
    </fieldset>
    <fieldset class="deliverytime">
        time delivery block
    </fieldset>

    <p class="freedelivery-block">Закажи еще на 123 грн и получи бесплатную доставку!</p>
    <input type="submit" id="order-submit" value="Оформить заказ" class="cart-ordersubmit">
</form>

</div>
</div>

<?= CartWidget::widget() ?>
