<?php

use frontend\widgets\category\CategoryWidget;
use frontend\widgets\header\HeaderWidget;
use frontend\widgets\cart\CartWidget;
use frontend\widgets\delivery\DeliveryWidget;

$this->title = 'BBQ-меню';
?>



<?= HeaderWidget::widget(['headerClass' => 'menupage', 'slogan' => 'Меню']) ?> 

<?= CategoryWidget::widget() ?>

<section class="bbq-breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs-top">
            <li><a href="index.html">Главная</a></li>
            <li><a href="menu.html">Меню</a></li>
            <li>Бургеры</li>
        </ul>
    </div>
</section>

<section class="bbq-menusection">
    <div class="container clr-white">
        <div class="row">
            <div class="col-md-6">
                    <div class="menusection-itemblock">
                        <img src="/img/burger-doubleblock.jpg" alt="bbq-menu" class="img-responsive">
                        <div class="menu-ovelayblock">
                            <a href="dish-singlepage.html">Все блюда</a>
                        </div>
                    </div>
                    <h3 class="bbqmenu-itemname">Бургеры</h3>
            </div>

            <div class="col-md-3">
                <div class="menusection-itemblock">
                    <img src="/img/kithen-menu.jpg" alt="bbq-menu" class="img-responsive">
                    <div class="menu-ovelayblock">
                        <a href="dish-singlepage.html">Все блюда</a>
                    </div>
                </div>
                <h3 class="bbqmenu-itemname">Кухня</h3>
            </div>

            <div class="col-md-3">
                <div class="menusection-itemblock">
                    <img src="/img/soup.jpg" alt="bbq-menu" class="img-responsive">
                    <div class="menu-ovelayblock">
                        <a href="dish-singlepage.html">Все блюда</a>
                    </div>
                </div>
                <h3 class="bbqmenu-itemname">Первые блюда</h3>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                    <div class="menusection-itemblock">
                        <img src="/img/kithen-menu.jpg" alt="bbq-menu" class="img-responsive">
                        <div class="menu-ovelayblock">
                            <a href="dish-singlepage.html">Все блюда</a>
                        </div>
                    </div>
                    <h3 class="bbqmenu-itemname">Бизнес-меню</h3>
            </div>

            <div class="col-md-3">
                <div class="menusection-itemblock">
                    <img src="/img/kithen-menu.jpg" alt="bbq-menu" class="img-responsive">
                    <div class="menu-ovelayblock">
                        <a href="dish-singlepage.html">Все блюда</a>
                    </div>
                </div>
                <h3 class="bbqmenu-itemname">Мангал</h3>
            </div>

            <div class="col-md-3">
                <div class="menusection-itemblock">
                    <img src="/img/soup.jpg" alt="bbq-menu" class="img-responsive">
                    <div class="menu-ovelayblock">
                        <a href="dish-singlepage.html">Все блюда</a>
                    </div>
                </div>
                <h3 class="bbqmenu-itemname">Десерты</h3>
            </div>

            <div class="col-md-3">
                <div class="menusection-itemblock">
                    <img src="/img/drink.jpg" alt="bbq-menu" class="img-responsive">
                    <div class="menu-ovelayblock">
                        <a href="dish-singlepage.html">Все блюда</a>
                    </div>
                </div>
                <h3 class="bbqmenu-itemname">Напитки</h3>
            </div>
        </div>
    </div>
</section>

<section class="popular-bbq">
    <div class="container nopadding">
        <h2 class="bbq-header recommended-bbq">Рекомендуемые блюда</h2>
    <div class="grid-wrap cols-four">
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-01.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Сыр на мангале с лимонно-медовой заправкой</h3>
            <span class="recommended-price">29 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-02.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Бургеры</span>
            <h3>Куриный бургер-сет</h3>
            <span class="recommended-price">60 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер - он великолепен</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="dish-singlepage.html"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер на мангале с лимонно - медовой заправкой и специями и прочий очень длинный текст который очень любят заказчики</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
    </div>

    </div>
</section>

<?= CartWidget::widget() ?>

<?= DeliveryWidget::widget() ?>