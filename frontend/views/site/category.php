<?php

use frontend\widgets\category\CategoryWidget;
use frontend\widgets\header\HeaderWidget;
use frontend\widgets\cart\CartWidget;
use frontend\widgets\delivery\DeliveryWidget;

$this->title = 'BBQ-категория';
?>



<?= HeaderWidget::widget(['headerClass' => 'menupage', 'slogan' => 'Бургеры']) ?> 

<?= CategoryWidget::widget() ?>

<section class="bbq-breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs-top">
            <li><a href="index.html">Главная</a></li>
            <li><a href="menu.html">Меню</a></li>
            <li>Бургеры</li>
        </ul>
    </div>
</section>

<section class="popular-bbq">
    <div class="container nopadding">
        <h2 class="bbq-header recommended-bbq">Рекомендуемые блюда</h2>
        <div class="row item-horisontal">
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 flexwrap">
                <div class="halfcol">
                    <a href="dish-singlepage.html" class="toitem"><img src="/img/cart-02.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
                </div>
                <div class="halfcol">
                    <p class="item-name">Сет «Z-Бургер»</p>
                    <p class="item-bbqcomposition">Состав: бургер, картофель, сметанный соус</p>
                    <p class="itembbqparameters">
                        <span>400 грамм</span>
                        <span>от 110 грн</span>
                    </p>
                    <div class="counter">
                        <button type="button" class="but counterBut dec">-</button>
                        <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                        <button type="button" class="but counterBut inc">+</button>
                    </div>
                    <a href="#" class="bbq-item-order">Заказать</a>
                </div>
            </div>

            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 flexwrap">
                <div class="halfcol">
                    <a href="dish-singlepage.html" class="toitem"><img src="/img/bitmap.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
                </div>
                <div class="halfcol">
                    <p class="item-name">Бургер с мясом и сыром «Чеддер»</p>
                    <p class="item-bbqcomposition">Состав: бургер, картофель, сметанный соус</p>
                    <p class="itembbqparameters">
                        <span>от 310 грамм</span>
                        <span>от 80 грн</span>
                    </p>
                    <div class="counter">
                        <button type="button" class="but counterBut dec">-</button>
                        <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                        <button type="button" class="but counterBut inc">+</button>
                    </div>
                    <a href="#" class="bbq-item-order">Заказать</a>
                </div>
            </div>


            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 flexwrap">
                <div class="halfcol">
                    <a href="dish-singlepage.html" class="toitem"><img src="/img/burger2.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
                </div>
                <div class="halfcol">
                    <p class="item-name">Дабл-бургер</p>
                    <p class="item-bbqcomposition">Состав: двойной сыр, две котлеты</p>
                    <p class="itembbqparameters">
                        <span>200 грамм</span>
                        <span>от 110 грн</span>
                    </p>
                    <div class="counter">
                        <button type="button" class="but counterBut dec">-</button>
                        <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                        <button type="button" class="but counterBut inc">+</button>
                    </div>
                    <a href="#" class="bbq-item-order">Заказать</a>
                </div>
            </div>
        </div>

    </div>
</section>

<!-- GMAP KEY   AIzaSyAfS_4_tPY7zFaLTVnHdY9QmyYGKU3TZxE  -->

<?= CartWidget::widget() ?>

<?= DeliveryWidget::widget() ?>





