<?php

use frontend\widgets\category\CategoryWidget;
use frontend\widgets\header\HeaderWidget;
use frontend\widgets\cart\CartWidget;
use frontend\widgets\delivery\DeliveryWidget;

$this->title = 'BBQ-категория';
?>



<?= HeaderWidget::widget(['headerClass' => 'menupage-oneitem']) ?> 

<?= CategoryWidget::widget() ?>

<section class="bbq-breadcrumbs">
    <div class="container">
        <ul class="breadcrumbs-top">
            <li><a href="index.html">Главная</a></li>
            <li><a href="menu.html">Меню</a></li>
            <li>Бургеры</li>
        </ul>
    </div>
</section>

<section class="bbq-singleitem">
    <div class="container clr-white">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <img src="/img/bbq-single.jpg" alt="bbq-oneitem" class="img-responsive singlebbq">
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                <h2 class="bbqsingle-itemhead">Бургер с мясом и сыром «Чеддер»</h2>
                <p class="bbqsingle-itemhead-weight">от 310 грамм</p>
                <p class="bbqsingle-item-text">Лень приготовить еду самому?</p>
                <p class="bbqsingle-item-text">Покупайте Бургер с мясом и сыром «Чеддер» из заведения "БарбеQ".
                Это вкусное, сытное, а также полезное блюдо, приготовленное из натуральных продуктов.</p>

                <div class="bbqsingle-bottom-wrapper">
                    <span class="oneprice">от 80 грн</span>

                    <div class="counter">
                        <button type="button" class="but counterBut dec">-</button>
                        <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                        <button type="button" class="but counterBut inc">+</button>
                    </div>
                    <a href="#" class="btn-singleorder">Заказать</a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                <div class="consist">
                    <h3>Состав</h3>
                    <ul>
                        <li>булочка</li>
                        <li>котлета</li>
                        <li>сыр «Чеддер»</li>
                        <li>огурец</li>
                        <li>листья салата</li>
                        <li>лук</li>
                        <li>соус</li>
                        <li>кетчуп</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="popular-bbq">
    <div class="container nopadding">
        <h2 class="bbq-header recommended-bbq">Рекомендуемые блюда</h2>
    <div class="grid-wrap cols-four">
        <div class="grid-item">
            <a href="#"><img src="/img/cart-01.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Сыр на мангале с лимонно-медовой заправкой</h3>
            <span class="recommended-price">29 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="#"><img src="/img/cart-02.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Бургеры</span>
            <h3>Куриный бургер-сет</h3>
            <span class="recommended-price">60 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
        <div class="grid-item">
            <a href="#"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="#"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="#"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер - он великолепен</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="#"><img src="/img/soup.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="#"><img src="/img/cart-01.jpg" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>

        <div class="grid-item">
            <a href="#"><img src="/img/cart-03.png" alt="slider-img" title="розовый динозаврик" class="img-responsive"></a>
            <span class="bbq-category">Мангал</span>
            <h3>Большой денер на мангале с лимонно - медовой заправкой и специями и прочий очень длинный текст который очень любят заказчики</h3>
            <span class="recommended-price">39 грн</span>

            <div class="counter">
                <button type="button" class="but counterBut dec">-</button>
                <input type="text" class="field fieldCount" value="1" data-min="1" data-max="20">
                <button type="button" class="but counterBut inc">+</button>
            </div>
            <a href="#" class="bbq-item-order">Заказать</a>
        </div>
    </div>

    </div>
</section>

<?= CartWidget::widget() ?>

<?= DeliveryWidget::widget() ?>