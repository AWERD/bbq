<?php

namespace common\components\site_settings;

use Yii;
use yii\base\Component;
use yii\caching\TagDependency;
use common\models\SiteSettings;
use common\models\Phones;

class SiteSettingsComponent extends Component
{

    public function data()
    {
        return $this->QuerySettings();
    }

    public function phones()
    {
        return $this->QueryPhones();
    }

    protected function QuerySettings()
    {
        $cached_query = SiteSettings::getDb()->cache(function ($db) {
            return SiteSettings::findOne(1);
        }, 0, new TagDependency(['tags' => 'site_settings']));

        return $cached_query;
    }

    protected function QueryPhones()
    {
        $cached_query = Phones::getDb()->cache(function ($db) {
            return Phones::find()->orderBy(['created_at' => SORT_DESC])->all();
        }, 0, new TagDependency(['tags' => 'site_phones']));

        return $cached_query;
    }

}