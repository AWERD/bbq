<?php

namespace common\components\badges;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use common\models\BackCall;

class BadgesComponent extends Component
{

    public function backCallBadge()
    {
        return BackCall::find()->where(['status' => 0])->count();
    }

}