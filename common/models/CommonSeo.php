<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;


class CommonSeo extends ActiveRecord
{

    public static function tableName()
    {
        return 'common_seo';
    }

    public function rules()
    {
        return [
            [['fb_app_id', 'fb_admins', 'og_site_name', 'twitter_site'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'fb_app_id' => 'fb:app_id',
            'fb_admins' => 'fb:admins',
            'og_site_name' => 'og:site_name',
            'twitter_site' => 'twitter:site'
        ];
    }

}