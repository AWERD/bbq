<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;


class BackCall extends ActiveRecord
{

    public static function tableName()
    {
        return 'back_call';
    }

    public function rules()
    {
        return [
            [['name', 'phone'], 'required'],
            [['status'], 'integer'],
            [['name', 'phone'], 'string', 'max' => 255]
        ];
    }

    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'phone' => 'Номер телефона',
            'status' => 'Статус'
        ];
    }

}