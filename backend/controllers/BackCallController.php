<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use common\models\BackCall;
use backend\models\BackCallSearch;


class BackCallController extends Controller
{
    
    public function actionIndex()
    {
        $searchModel = new BackCallSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        
        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
    
    public function actionChangeStatus($id)
    {
        $model = BackCall::findOne($id);

        if($model){
            $model->status == 0 ? $model->status = 1 : $model->status = 0;

            if($model->save(false)){
                Yii::$app->session->setFlash('info', 'Статус успешно изменен!');

                return $this->redirect('index');
            }
        }
    }
    
    public function actionDelete($id)
    {
        $model = BackCall::findOne($id);
        
        if($model && $model->delete()){
            Yii::$app->session->setFlash('danger', 'Запись успешно удалена!');
            
            return $this->redirect('index');
        } else {
            Yii::$app->session->setFlash('danger', 'Запись не найдена!');

            return $this->redirect('index');
        }
    }

}
