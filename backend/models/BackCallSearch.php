<?php
namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\BackCall;

class BackCallSearch extends Model
{

    public $search;

    public function rules()
    {
        return [
            [['search'], 'string'],
        ];
    }

    public function search($params)
    {
        $query = BackCall::find()->orderBy('status');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $this->load($params);

        $query->andFilterWhere(['like', 'name', $this->search])
            ->orFilterWhere(['like', 'phone', $this->search]);

        return $dataProvider;
    }
}