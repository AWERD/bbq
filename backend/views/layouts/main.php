<?php

use yii\helpers\Html;
use yii\widgets\Pjax;
use common\widgets\Alert;
use backend\assets\AppAsset;
use common\models\Profile;

$profile = Profile::findOne(['user_id' => Yii::$app->user->getId()]);

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="navbar navbar-default header-highlight pjax-load">
    <?= $this->render('_top_menu', [
        'profile' => $profile
    ]) ?>
</div>


<div class="page-container">
    <?php Pjax::begin([
        'id' => 'dynamic-pjax',
        'formSelector' => '.pjax-load',
        'linkSelector' => '.pjax-link',
        'options' => [
            'class' => 'page-content'
        ]
    ]) ?>
        <div id="pjax-load" class="sidebar sidebar-main pjax-load">
            <?= $this->render('_menu', [
                'profile' => $profile
            ]) ?>
        </div>

        <div class="content-wrapper">
            <div class="content pjax-load">
                <?= Alert::widget() ?>
                <?= $content ?>
            </div>
        </div>
    <?php Pjax::end() ?>
</div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
