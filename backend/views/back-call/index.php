<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Обратные звонки';
?>

<h2 style="text-align: center">Список обратных звонков</h2>

<div class="row">
    <div class="col-md-6">
        <?php Pjax::begin(['id' => 'search']) ?>
            <?php $form = ActiveForm::begin(['options' => ['data-pjax' => true ], 'method' => 'get', 'action' => ['index']]); ?>
                <div class="form-group has-feedback has-feedback-left" style="max-width: 300px;">
                    <input class="form-control ui-autocomplete-input" name="BackCallSearch[search]" placeholder="Поиск" id="ac-basic" autocomplete="off" type="text">
                    <div class="form-control-feedback">
                        <i class="icon-search4 text-size-base"></i>
                    </div>
                </div>
            <?php ActiveForm::end() ?>
        <?php Pjax::end() ?>
    </div>
</div>

<?php Pjax::begin(['id' => 'grid']) ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout'=>"{items}\n{pager}\n{summary}",
        'columns' => [
            [
                'class' => 'yii\grid\SerialColumn',
                'header' => '#',
            ],
            [
                'header' => 'Имя',
                'value' => 'name'
            ],
            [
                'header' => 'Телефон',
                'value' => 'phone'
            ],
            [
                'header' => 'Дата',
                'value' => function($model){
                    return date('d.m.Y H:i', strtotime($model->created_at));
                }
            ],
            [
                'header' => 'Статус',
                'format' => 'raw',
                'value' => function($model){
                    if($model->status == 0) return '<span class="label label-info">В ожидании</span>';
                    if($model->status == 1) return '<span class="label label-success">Принят</span>';
                }
            ],
            [
                'header' => 'Действия',
                'format' => 'raw',
                'value' => function($model){
                    return
                        Html::a('<span class="icon-pencil6"></span>', ['change-status', 'id' => $model->id], ['class' => 'pjax-link', 'title' => 'Изменить статус']).' '.
                        Html::a('<span class="icon-trash"></span>', ['delete', 'id' => $model->id],
                            [
                                'class' => 'pjax-link',
                                'title' => 'Удалить',
                                'data' => [
                                    'confirm' => 'Вы уверены что хотите удалить запись?'
                                ]
                            ]);
                }
            ]
        ],
    ]); ?>
<?php Pjax::end() ?>