<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

$this->title = 'Общее SEO';

?>

<div class="col-md-4">
    <?php Pjax::begin() ?>
        <?php $form = ActiveForm::begin([
            'id' => 'common-seo-form',
            'options' => [
                'data-pjax' => true
            ]
        ]); ?>
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h5 class="panel-title">Общие SEO данные страниц сайта</h5>
                <div class="heading-elements">
                    <ul class="icons-list">
                        <li><a data-action="collapse"></a></li>
                    </ul>
                </div>
            </div>

            <div class="panel-body">
                <fieldset>
                    <?= $form->field($model, 'fb_app_id')->textInput(['class' => 'form-control']) ?>
                    <?= $form->field($model, 'fb_admins')->textInput(['class' => 'form-control']) ?>
                    <?= $form->field($model, 'og_site_name')->textInput(['class' => 'form-control']) ?>
                    <?= $form->field($model, 'twitter_site')->textInput(['class' => 'form-control']) ?>
                </fieldset>

                <div class="text-right">
                    <button type="submit" class="btn btn-primary">Сохранить <i class="icon-arrow-right14 position-right"></i></button>
                </div>
            </div>
        </div>
        <?php ActiveForm::end(); ?>
    <?php Pjax::end() ?>
</div>
